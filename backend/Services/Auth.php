<?php
namespace App\Services;
use App\Models\Token;
use App\Models\User;

use Exception;

class Authunticat
{
    static public  function getAuthunticatedUser()
    {
        $request = Request();
        
        if (!$request) {
            throw new Exception("Unable to verify auth!");
        }
        
        $token = $request->token;

        if (!$token) {
            throw new Exception("Token is not available");
        }

        $userToken = Token::where('key', $token)->first();

        if (!$userToken) {
            throw new Exception("Unauthorized!");
        }

        return User::where('id', $userToken->id)->first();
    }
    
}