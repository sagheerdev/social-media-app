<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PriceController;
use App\Http\Controllers\ProductController;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[AdminController::class,'login']);
Route::post('logout',[AdminController::class,'logout']);



Route::post('getProducts',[ProductController::class,'getProducts']);
// Route::post('placeOrder',[OrderController::class,'placeOrder']);

Route::post('getOrders',[OrderController::class,'getOrders']);


Route::post('getAllOrders',[OrderController::class,'getAllOrders']);
Route::post('getTodayOrder',[OrderController::class,'getTodayOrder']);

Route::post('getStatus',[OrderController::class,'getStatsV2']);
Route::post('addProduct',[ProductController::class,'addProduct']);

Route::post('getProductData/{id}',[ProductController::class,'getProductData']);
Route::post('updateProduct/{id}',[ProductController::class,'updateProduct']);
Route::delete('deleteProduct/{id}',[ProductController::class,'deleteProduct']);




// Route::post('getOrderStatus',[OrderStatusController::class,'getOrderStatus']);
// Route::delete('deleteProduct/{id}',[ProductController::class,'deleteProduct']);
// Route::post('searchBar/{name}',[ProductController::class,'searchBar']);