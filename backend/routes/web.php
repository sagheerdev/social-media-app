<?php

use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/place-order',[OrderController::class,'placeOrder']);
Route::get('/order-placed-successfully',[OrderController::class,'orderPlacesSuccessfully'])
->name("orderPlacesSuccessfully");
Route::get('/order-done',[OrderController::class,'orderdone']);

Route::get('/order-canceled',[OrderController::class,'orderCanceled'])
->name("orderCanceled");



