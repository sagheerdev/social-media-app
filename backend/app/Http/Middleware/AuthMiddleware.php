<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Authunticat as ServicesAuth;
use Illuminate\Http\Request;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
      
        if(ServicesAuth::getAuthunticatedUser())
        {
            return $next($request);
        }
        return response()->json(['message'=>'Not Valid']);

        
    }
}
