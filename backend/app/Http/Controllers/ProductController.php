<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Facades\File;
use App\Models\Price;
use Error;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function getProducts()
    {
        $products = Product::query()
            ->with('prices')
            ->get();
        return response()->json(['message' => 'Product data here', 'products' => $products]);
    }

    public function addProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ["bail", "required", "string"],
            'description' => ["bail", "required", "string"],
            'features' => ["bail", "required", "array"],
            'image_data' => ["bail", "required"],
            'prices' => ["bail", "required", "array"],
            'prices.*.name' => ["bail", "required", "string"],
            'prices.*.amount' => ["bail", "required", "integer"],
            'prices.*.api_service_id' => ["bail", "required", "integer"],
            'prices.*.api_quantity' => ["bail", "required", "integer"],
        ]);


        if ($validator->fails()) {
            $responce = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($responce, 401);
        }
        try {
            DB::beginTransaction();
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->features = $request->features;

            $uploadImage = ProductController::uploadFile64bit($request, 'image_data', null, 'images/');

            $product->image  = data_get($uploadImage, 'file_path');

            $product->save();
            foreach ($request->prices as $price) {
                Price::create([
                    'product_id' => $product->id,
                    'name' => $price['name'],
                    'amount' => $price['amount'],
                    'api_service_id' => $price['api_service_id'],
                    'api_quantity' => $price['api_quantity'],
                    'strip_price_id' => $price['strip_price_id']
                ]);
            }
            DB::commit();
        } catch (Exception | Error) {
            DB::rollBack();
        }

        $product = Product::with(['prices'])->first();
        return response()->json([
            'message' => 'Product created Successfull',
            'Product' => $product
        ], 200);
    }

    //update Product
    public function getProductData($id)
    {
        $product = Product::query()
            ->find($id)
            ->with('prices')
            ->first();

        return response()->json([
            'message' => 'product data here',
            'product' => $product
        ], 200);
    }

    //update products
    public function updateProduct(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ["bail", "required", "string"],
            'description' => ["bail", "required", "string"],
            'features' => ["bail", "required", "array"],
            'image_data' => ["bail"],
            'prices' => ["bail", "required", "array"],
            'prices.*.name' => ["bail", "required", "string"],
            'prices.*.amount' => ["bail", "required", "integer"],
            'prices.*.api_service_id' => ["bail", "required", "integer"],
            'prices.*.api_quantity' => ["bail", "required", "integer"],
            'prices.*.strip_price_id' => ["bail", "required", "string"],
        ]);

        if ($validator->fails()) {
            $responce = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($responce, 401);
        }

        try {
            DB::beginTransaction();

            $product = Product::find($request->id);
            $product->name = $request->name;
            $product->description = $request->description;
            $product->features = $request->features;
            $product->save();

            if ($request->image_data) {
                $uploadImage = ProductController::uploadFile64bit($request, 'image_data', null, 'images/');
                $product->image = data_get($uploadImage, 'file_path');
                $product->save();
            }

            foreach ($request->prices as $priceData) {
                $priceId  = data_get($priceData, 'id') ?? null;

                if ($priceId) {
                    $price = Price::where('id', $priceId)->first();
                } else {
                    $price = new Price();
                }
                // dd($price,$priceData);

                $price->product_id = $product->id;
                $price->name = $priceData['name'];
                $price->amount = $priceData['amount'];
                $price->api_service_id = $priceData['api_service_id'];
                $price->api_quantity = $priceData['api_quantity'];
                $price->strip_price_id = $priceData['strip_price_id'];
                $price->save();
            }

            if ($request->prices && count($request->prices)) {
                $priceIds = collect($request->prices)
                    ->filter(function ($price) {
                        return !!data_get($price,'id');
                    })
                    ->pluck('id')
                    ->toArray();

                $deleteAbelPrices = $product->prices()->whereNotIn('id', $priceIds)->get();

                foreach ($deleteAbelPrices as $deleteAbelPrice) {
                    $deleteAbelPrice->delete();
                }
            }

            DB::commit();
            $product = Product::with(['prices'])->where('id', $product->id)->first();
            return response()->json([
                'message' => 'Product update Successfull',
                'Product' => $product
            ], 200);
        } catch (Exception | Error $e) {
            DB::rollBack();
            return response()->json([
                'message' => $e->getMessage(),
            ], 401);
        }
    }
    // Delete Product
    public function deleteProduct($id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json(['This product Id is not exist']);
        }
        $product->delete();
        return response()->json(['message' => 'Product Delete Sunccessfully']);
    }

    // public function searchBar($name)
    // {
    //     $product = Product::query()
    //     ->where('name','like','%'.$name.'%')
    //     ->get();

    //     return response()->json(['message'=>'Here Your Search Result',$product]);
    // }

    public static function uploadFile64bit(Request $request, $requestName = 'fileData', $fileName = null, $uploadPath = 'uploads/')
    {
        try {
            $image_64 = $request->$requestName;

            if (!$image_64) {
                return false;
            }

            // decode the base64 file
            $file = base64_decode(preg_replace(
                '#^data:image/\w+;base64,#i',
                '',
                $request->input($requestName)
            ));

            if (in_array($file, ["", null, ' '])) {
                return false;
            }

            // get file details
            $extension = '.' . explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
            $fileOriginalName = Str::random(10);

            if (!$fileName) {
                $fileName = $fileOriginalName . '_' . time() . '.' . $extension;
            }

            // create upload directory if not exists
            if (!File::exists(public_path($uploadPath))) {
                File::makeDirectory(public_path($uploadPath), 0777, true);
            }

            $filePath = $uploadPath . '' . $fileName;

            // upload File to the server
            $moved = File::put(public_path($filePath), $file);

            // error if file upload fails
            if (!$moved) {
                throw new Exception('Unable To Upload File: ' . $fileName);
            }

            $uploadedFilePath = '/' . $uploadPath . $fileName;

            $response = [
                'file_name' => $fileName,
                'file_path' => $uploadedFilePath,
                'file_url' => url($uploadedFilePath),
            ];

            // dd($response);
            return $response;
        } catch (Exception $e) {
            // Inspector::reportException($e);
            throw new Exception($e->getMessage());
        }
    }
}