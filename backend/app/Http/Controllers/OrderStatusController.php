<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class OrderStatusController extends Controller
{
    public function getOrderStatus()
    {
        $api_url ='https://justanotherpanel.com/api/v2';

        $response = Http::post($api_url,[
            'key'=>'430bae88964eae308b40f0744ecbff4d',
            'action'=>'status',
            'order'=>390319165
        ])->json();
       
        $orderCharge = data_get($response, 'charge');
        $startCount = data_get($response, 'start_count');
        $status = data_get($response, 'status');
        $remains =data_get($response, 'remains');
        $currency =data_get($response, 'currency');
    

        $order = new OrderStatus();
        $order->api_charge = $orderCharge;
        $order->api_start_count = $startCount;
        $order->api_status = $status;
        $order->api_remains = $remains;
        $order->api_currency = $currency;
        $order->save();
        // dd($order);  
        return response()->json(['message'=>'Order Status', 'order'=>$order]);
    }
    
}
