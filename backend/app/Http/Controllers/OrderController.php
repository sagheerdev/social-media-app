<?php

namespace App\Http\Controllers;

use App\Mail\OrderProcess;
use App\Mail\OrderStatus;
use App\Models\Order;
use App\Models\Price;
use App\Models\Product;
use Stripe\Stripe;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laravel\Cashier\Cashier;

class OrderController extends Controller
{
    public function placeOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [

            "page_url" => ["bail", "required", "string"],
            "product_id" => ["bail", "required", "numeric", "exists:products,id"],
            "price_id" => ["bail", "required", "numeric", "exists:prices,id"],
            "first_name" => ["bail", "required", "string"],
            "last_name" => ["bail", "required", "string"],
            "email" => ["bail", "required"]
        ]);

        if ($validator->fails()) {
            $responce = [
                'success' => false,
                'message' => $validator->errors()
            ];
            return response()->json($responce, 401);
        }

        $price = Price::query()
            ->where('product_id', $request->product_id)
            ->where('id', $request->price_id)
            ->first();

        if (!$price) {
            $responce = [
                'success' => false,
                'message' => "Invalid Product or Price Option!"
            ];
            return response()->json($responce, 401);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            $user = new User();
        }
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->save();

        return $user->checkout([$price->strip_price_id => 1], [
            'success_url' => route('orderPlacesSuccessfully', [
                'user_id' => $user->id,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'price_id' => $price->id,
                'api_service_id' => $price->api_service_id,
                'api_quantity' => $price->api_quantity,
                'product_id' => $price->product_id,
                'page_url' => $request->page_url,
                'email' => $request->email
            ]),
            'cancel_url' => route('orderCanceled'),
        ]);
    }

    public function orderPlacesSuccessfully(Request $request)
    {
        $price = Price::query()
            ->where('id', $request->price_id)
            ->first();

        $product = Product::query()
        ->where('id', $request->product_id)
        ->first();

        //Placed Order
        $api_url = "https://justanotherpanel.com/api/v2";
        $responce = Http::post($api_url, [

            'key' => "b5e3bea5180ca2a039f6c4cbd316b0f6",
            'action' => 'add',
            'service' => $price->api_service_id,
            'link' => "https://instagram.com/quilted_bedding?igshid=YmMyMTA2M2Y=",
            "quantity" => $price->api_quantity
        ])
            // ->throw()
            ->json();

        $orderId = data_get($responce, 'order');

        $error = data_get($responce, 'error');

        $status = "Processing";

        $orderPlacedOnJustAnOtherPanel =false;
        
        if (!!$error || !$orderId) {
        } else {
            //Order Status 
            $api_url = 'https://justanotherpanel.com/api/v2';

            $response = Http::post($api_url, [
                'key' => 'b5e3bea5180ca2a039f6c4cbd316b0f6',
                'action' => 'status',
                'order' => 390319165
            ])->json();
            $status = data_get($response, 'status',);
            
            $orderPlacedOnJustAnOtherPanel = true;
        }

        
        $newOrder = new Order();

        $newOrder->api_order_id = $orderId;
        $newOrder->page_url = $request->page_url;
        $newOrder->product_id = $request->product_id;
        $newOrder->amount = $price->amount;
        $newOrder->first_name = $request->first_name;
        $newOrder->last_name = $request->last_name;
        $newOrder->api_order_status = $status;
        $newOrder->email = $request->email;

        // dd($newOrder);        
        $newOrder->save();


        if($orderPlacedOnJustAnOtherPanel == false){
            Mail::to(['admin@gmail.com'])
            ->send(new OrderProcess($newOrder));
        }
                    
        Mail::to(['email' => $request->email])->send(new OrderStatus($newOrder,$price,$product));


        return redirect('order-done');
    }

    public function orderdone()
    {
        return view('orderDone');
    }

    // public function orderCanceled()
    // {
    //     return view('orderCancled');
    // }
    // get All orders

    public function getOrders()
    {

        $todayOrder = Carbon::now();
        $yesterday = Carbon::yesterday();
        $thirtydays = Carbon::now()->subDays(30);
    

        $todayOrder = Order::query()
            ->with('product')
            ->whereDate('created_at', $yesterday)
            ->get();

        
        $thirtydays = Order::query()
            ->with('product')
            ->where('created_at', '>', $thirtydays)
            ->get();

        $orders = [
            'Today_Order' => $todayOrder,
            'Yesterday_Order' => $yesterday,
            'Month_Order' => $thirtydays,
        ];
        return response()->json(['message' => 'Orders Details', 'order' => $orders]);
    }


    public function getAllOrders()
    {

        $todayOrder = Carbon::now();
        $yesterday = Carbon::yesterday();
        $thirtydays = Carbon::now()->subDays(30);


        $todayOrder = Order::query()
            ->with('product')
            ->whereDate('created_at', $todayOrder)
            ->get();

        $yesterday = Order::query()
            ->with('product')
            ->whereDate('created_at', $yesterday)
            ->get();

        $thirtydays = Order::query()
            ->with('product')
            ->whereDate('created_at', '>', $thirtydays)
            ->get();

        $previousMonthDate = Carbon::now()->lastOfMonth();
        $previous = Order::query()
            ->with('product')
            ->whereDate('created_at', '<', $previousMonthDate)
            ->get();

        $orders = [
            'Today_Order' => $todayOrder,
            'Yesterday_Order' => $yesterday,
            'Month_Order' => $thirtydays,
            'Previous_Order' => $previous,
        ];
        return response()->json(['message' => 'Orders Details', 'order' => $orders]);
    }

    // get Complete Orders
    public function getStatus()
    {
        $completeOrdersCount = Order::query()
            ->where('api_order_status', '=', 'Completed')
            ->count();

        $pendingOrdersCount = Order::query()
            ->where('api_order_status', '=', 'Pending')
            ->count();

        $canceledOrdersCount = Order::query()
            ->where('api_order_status', '=', 'Canceled')
            ->count();

        $resData = [
            'complete_orders_count' => $completeOrdersCount,
            'pending_orders_count' => $pendingOrdersCount,
            'canceled_orders_count' => $canceledOrdersCount,
        ];


        return response()->json([
            'message' => 'Completed Orders',
            'orders' => $resData
        ], 200);
    }

    public function getStatsV2()
    {
        $orderStatusTypesList = [
            "Completed",
            "Pending",
            "Canceled",
            // "Inprogress",
            // "Partial",
            // "Processing",
        ];

        $resData = [];

        foreach ($orderStatusTypesList as $statusType) {

            $count = Order::query()
                ->where('api_order_status', $statusType)
                ->count();
            $resData["{$statusType}_orders_count"] = $count;
        }

        return response()->json([
            'message' => 'Orders Status',
            'orders' => $resData
        ], 200);
    }
}
