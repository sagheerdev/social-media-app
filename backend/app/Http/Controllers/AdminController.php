<?php

namespace App\Http\Controllers;

use App\Models\Token;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email'=>'required|email',
            'password'=>'required'
        ]);

        if($validator->fails())
        {
            $response = [
                'success'=>false,
                'message'=>$validator->errors()
            ];
            return response()->json($response,401);
        }

        $user = User::where('email',$request->email)->first();
        if(!$user)
        {
            return response()->json(['message'=>'Invalid Credentials'],401);
        }
        if(!Hash::check($request->password,$user->password))
        {
            return response()->json(['message'=>'Invalid Credentials'],401);
        }

            $token = Str::random(32);
            $newToken = new Token();
            $newToken->key = $token;
            $newToken->save();

            return response()->json([
                'message'=>'Admin login Successfully',
                'user'=>$user, 
                'key'=>$token
            ],200);
    }

    public function logout(Request $request)
    {
        // $validator = Validator::make($request->all(),[
        //     'key'=>'required',
        // ]);

        // if($validator->fails())
        // {
        //     $response = [
        //         'success'=>false,
        //         'message'=>$validator->errors()
        //     ];
        //     return response()->json($response,401);
        // }

        $key = Token::where('key',$request->key)->first();
        if(!$key)
        {
            return response()->json(['message'=>'Enter The Valid Key'],401);
        }

        $token = Token::where('key', $request->key)->first();

        if ($token) {
            $token->delete();
        }

        return response()->json(['message' => 'user logout successfully']);
    }

}
