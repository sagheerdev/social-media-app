<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'page_url',
        'first_name',
        'last_name',
        'api_order_status',
        'email'
    ];
    protected $hidden = [

    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function price()
    {
        return $this->hasMany(Price::class);
    }
}
