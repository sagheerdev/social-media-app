<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Price;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'image',
        
    ];

    public function getFeaturesAttribute($value)
    {
        return json_decode($value,true);
    }
 
    public function setFeaturesAttribute($value)
    {
        $this->attributes['features'] = json_encode($value);
    }
    

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function prices()
    {
        return $this->hasMany(Price::class);
    }
}
