<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Price extends Model
{
    public $guarded  = [];
    
    use HasFactory;
    protected $fillable = [
        'product_id',
        'name',
        'amount',
        'api_service_id',
        'api_quantity',
        'strip_price_id'
    ];
    
    public function price()
    {
        return $this->belongsTo(Product::class);
    }
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

}
