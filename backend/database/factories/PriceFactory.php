<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Price>
 */
class PriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'quantity'=>fake()->quantity,
            'product_id'=>fake()->product_id,
            'price'=>fake()->price,
            'api_price-id'=>fake()->api_price_id
        ];
    }
}
