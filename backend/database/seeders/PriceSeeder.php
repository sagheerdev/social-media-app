<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prices')->insert([
            [
                'product_id' => 1,
                'name' => '1000 likes $4',
                'amount' => 4,
                'api_service_id' => 1724,
                'api_quantity' => 1000,
                'strip_price_id'=>'price_1Mh7OWHV60JH3jn5jVmj02A9'
            ],
            [
                'product_id' => 1,
                'name' => '2000 likes $8',
                'amount' => 8,
                'api_service_id' => 1724,
                'api_quantity' => 2000,
                'strip_price_id'=>'price_1MhaENHV60JH3jn5Rc0IIFlZ'
            ],
            [
                'product_id' => 1,
                'name' => '3000 likes $20',
                'amount' => 20,
                'api_service_id' => 1724,
                'api_quantity' => 3000,
                'strip_price_id'=>'price_1MiZNGHV60JH3jn5nl4VmKEX'
            ],
            [
                'product_id' => 2,
                'name' => '1000 Instagram Followers $5',
                'amount' => 5,
                'api_service_id' => 1724,
                'api_quantity' => 1000,
                'strip_price_id'=>'price_1MiZl3HV60JH3jn5nKYzXgIb'
            ],
            [
                'product_id' => 2,
                'name' => '2000 Instagram Followers $10',
                'amount' => 10,
                'api_service_id' => 1724,
                'api_quantity' => 2000,
                'strip_price_id'=>'price_1MiZlLHV60JH3jn5OLH3jDJg'
            ],
            [
                'product_id' => 2,
                'name' => '3000 Instagram Followers $20',
                'amount' => 20,
                'api_service_id' => 1724,
                'api_quantity' => 3000,
                'strip_price_id'=>'price_1MiZlVHV60JH3jn5Uzz14rlW'
            ],
            [
                'product_id' => 3,
                'name' => '1000 Instagram Followers $4',
                'amount' => 4,
                'api_service_id' => 1724,
                'api_quantity' => 1000,
                'strip_price_id'=>'price_1Mh7OWHV60JH3jn5jVmj02A9'
            ],
            [
                'product_id' => 3,
                'name' => '2000 Instagram Followers $8',
                'amount' => 8,
                'api_service_id' => 1724,
                'api_quantity' => 2000,
                'strip_price_id'=>'price_1MhaENHV60JH3jn5Rc0IIFlZ'
            ],
            [
                'product_id' => 3,
                'name' => '3000 Instagram Followers $20',
                'amount' => 20,
                'api_service_id' => 1724,
                'api_quantity' => 3000,
                'strip_price_id'=>'price_1MiZNGHV60JH3jn5nl4VmKEX'
            ],
            [
                'product_id' => 4,
                'name' => '1000 Instagram Followers $5',
                'amount' => 5,
                'api_service_id' => 1724,
                'api_quantity' => 1000,
                'strip_price_id'=>'price_1MiZl3HV60JH3jn5nKYzXgIb'
            ],
            [
                'product_id' => 4,
                'name' => '2000 Instagram Followers $10',
                'amount' => 10,
                'api_service_id' => 1724,
                'api_quantity' => 2000,
                'strip_price_id'=>'price_1MiZlLHV60JH3jn5OLH3jDJg'
            ],
            [
                'product_id' => 4,
                'name' => '3000 Instagram Followers $20',
                'amount' => 20,
                'api_service_id' => 1724,
                'api_quantity' => 3000,
                'strip_price_id'=>'price_1MiZlVHV60JH3jn5Uzz14rlW'
            ],
            
        ]);
    }
}
