<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productsList = [
            [
                'name'=>'Facebook Page Likes',
                'description'=>'Fast Delivery & Support 100% Authentic Engagement System',
                'features'=>['Real Likes','100% Safe','Money-Back Guarantee'],
                'image'=>'/images/Group 21.png',

            ],
            [
                'name'=>'Instagram Followers',
                'description'=>'Fast Delivery & Support 100% Authentic Engagement System',
                'features'=>['Real Likes','100% Safe','Money-Back Guarantee'],
                'image'=>'/images/Group 14.png'
            ],
            [
                'name'=>'Facebook Page Likes',
                'description'=>'Fast Delivery & Support 100% Authentic Engagement System',
                'features'=>['Real Likes','100% Safe','Money-Back Guarantee'],
                'image'=>'/images/Group 21.png'
            ],
            [
                'name'=>'Instagram Followers',
                'description'=>'Fast Delivery & Support 100% Authentic Engagement System',
                'features'=>['Real Likes','100% Safe','Money-Back Guarantee'],
                'image'=>'/images/Group 14.png'
            ],
        ];
        
        foreach($productsList as $product){
            $newProduct = new Product();

            $newProduct->name = $product['name'];
            $newProduct->description = $product['description'];
            $newProduct->features = $product['features'];
            $newProduct->image = $product['image'];
            $newProduct->save();
        }
        
    }
}
