#!/bin/bash
read -e -p " Full deploy? Y/N : " -i "N" isFullDeploy


echo "Deploying On Main."
livePath=/var/www/html/likes4u.co/public_html
gitPath=/var/www/html/likes4u.co/public_html/social-media-app
frontendBuildPath=/var/www/html/likes4u.co/public_html/social-media-app/frontend/dist
frontendPath=/var/www/html/likes4u.co/public_html/social-media-app/frontend
backendPath=/var/www/html/likes4u.co/public_html/social-media-app/backend

if [[ $isFullDeploy == "y" || $isFullDeploy == "Y" ]]
then

    # usermod -a -G www-data root
    # cd ${livePath}/ && chown -R www-data:root .

    cd ${gitPath}/ && sudo git stash;
    cd ${gitPath}/ && sudo git pull;
    # mQibfCyuxgPSCKsD7F9n

    'rm' ${livePath}/css -r
    'rm' ${livePath}/js -r

    # 'cp' ${frontendBuildPath}/* ${livePath}/ -rfv;

    # 'cp' ${frontendBuildPath}/.htaccess ${livePath}/.htaccess -rfv;

    'cp' ${backendPath}/server.env ${backendPath}/.env -rfv;
    
    # 'cp' ${backendPath}/server.pm2.config.js ${backendPath}/pm2.config.js -rfv;

    # 'rm' ${gitPath}/frontend/src -rf

    'rm' ${livePath}/backend
    
    'ln' -sf ${backendPath}/public ${livePath}/backend
    
    usermod -a -G www-data ubuntu
    cd ${livePath}/ && chown -R ubuntu:www-data .

    # find ${livePath}/ -type f -exec chmod 664 {} \;
    # find ${livePath}/ -type d -exec chmod 775 {} \;
    chmod 777 . -R
fi

cd ${backendPath}/ && php artisan optimize:clear;
# cd ${backendPath}/ && php artisan queue:restart;
# cd ${backendPath}/ && composer update;

if [[ $isFullDeploy == "n" || $isFullDeploy == "N" ]]
then
    
    # cd ${gitPath}/ && pm2 flush;

    # cd ${backendPath}/ && pm2 delete all;
    # cd ${frontendPath}/ && pm2 delete pm2.config.js;

    cd ${frontendPath}/ && yarn install;
    cd ${frontendPath}/ && yarn build;
    'cp' ${frontendBuildPath}/* ${livePath}/ -rfv;
    'cp' ${frontendBuildPath}/.htaccess ${livePath}/.htaccess -rfv;

    # cd ${backendPath}/ && pm2 restart pm2.config.js;
    # cd ${frontendPath}/ && pm2 restart pm2.config.js;
    # cd ${backendPath}/ && php artisan queue:restart;
fi

echo "Project Deploy done."

# cd /var/www/html/likes4u.co/public_html/social-media-app/
# sudo bash server.deploy.sh