// pm2 restart pm2.config.js
module.exports = {
  apps: [
    {
      name: "social-media-server",
      exec_mode: "fork",
      instances: "1",
      interpreter: "/bin/bash",
      script: "yarn",
      args: ["preview", "--port=4001"],
      restart_delay: 3000,
      max_memory_restart: "512M",
      // max_restarts: 1,
      autorestart: true,
      watch: false,
      error_file: "./logs/error-default.log",
      out_file: "./logs/out-default.log",
      pid_file: "./logs/default.pid",
      env: {
        NODE_ENV: "development",
        NODE_OPTIONS: `--max_old_space_size=1024M`,
      },
    },
  ],
};
