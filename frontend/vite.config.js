import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { fileURLToPath, URL } from "url";
import { splitVendorChunkPlugin } from "vite";
// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  return {
    // root: "./",
    // publicDir: "assets",
    base: command === "build" ? "https://likes4u.co/" : "/",
    build: {
      commonjsOptions: {
        transformMixedEsModules: true,
      },
    },
    plugins: [vue(), splitVendorChunkPlugin()],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
  };
});
