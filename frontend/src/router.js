import { createRouter, createWebHistory } from "vue-router";
import Index from "./Pages/index.vue";
import Login from "./Pages/AdminDashboard/Login.vue";
import AdminLayout from "./Layouts/AdminLayout.vue";
import Home from "./Pages/AdminDashboard/Home.vue";
import ProductManagement from "./Pages/AdminDashboard/ProductManagement.vue";
import OrderManagement from "./Pages/AdminDashboard/OrderManagement.vue";
const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: Index },
    {
      path: "/login",
      component: Login,
      meta: { requireAuth: true },
    },
    {
      path: "/admin",
      component: AdminLayout,
      meta: { adminonly: true },
      children: [
        {
          path: "",
          component: Home,
        },
        {
          path: "product",
          component: ProductManagement,
        },
        {
          path: "order",
          component: OrderManagement,
        },
      ],
    },
  ],
});
router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("admin-info")
    ? JSON.parse(localStorage.getItem("admin-info"))?.token
    : null;

  if (to.meta.requireAuth && token) {
    next("/admin");
  } else if (to.meta.adminonly && !token) {
    next("/login");
  } else {
    next();
  }
});
export default router;
