import { createApp } from 'vue'
import App from './App.vue'
import {createPinia} from 'pinia'
import router from './router'
import './index.css'
const pinia = createPinia()
import 'tw-elements'
createApp(App).use(router).use(pinia).mount('#app')
