const getApiPath = (url = "") => {
  if (window.location.hostname.includes("localhost")) {
    return `http://127.0.0.1:8000${url}`;
  }
  return `https://likes4u.co/backend${url}`;
};
export { getApiPath };
