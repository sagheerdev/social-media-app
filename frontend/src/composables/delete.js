import { getApiPath } from "./apiroute";

const del = async (url, products, headers) => {
  const baseUrl = getApiPath("/api");
  const fullUrl = baseUrl + url;
  const requestData = {
    ...products,
  };
  const response = await fetch(fullUrl, {
    method: "DELETE",
    body: JSON.stringify(requestData),
    headers: {
      ...headers,
      "content-type": "application/json",
    },
  });

  const jsonResponse = await response.json();

  return { data: jsonResponse };
};
export { del };
