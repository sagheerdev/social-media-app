module.exports = {
  content: ["./src/**/*.{html,js,vue}"],
  theme: {
    extend: {
      colors:{
        blue:{
          50: "#00ABE4",
          52:"#05bafa",
          53: "#083D77",
          54: "#E9F1FA"
        },
        gray:{
          50: "#E9F1FA",
          51: "#F5F5F5"
        },
        green:{
          base: "#58BB3F"
        },
        red:{
          base: "#F22501"
        },
        
        orange:{
          base: "#E89040"
        },
      },
      fontWeight: {
        extrabold: '900',
        custom: '400px'
      },
      boxShadow: {
        '3xl': '0px 4px 10px rgba(24, 76, 52, 0.25)',
        '4x1': '0px 4px 10px rgba(0, 171, 228, 0.25);'
      },
      borderRadius: {
      'lx': '6px'
      },
      fontSize: {
        '7xl': '32px',
        '9xl': '48px',
        '10xl': '64px',
        '1xl':  '15px',
        '2x2':  '22px',
        '4x2':  '40px'
      }, 
      opacity:{
        '10': '10%'
      },
      flexBasis:{
        '4x2':  '40px'
      },
      width: {
        '98': '36rem',
        "99": '634px',
        "58": '158px',
        "1440":'1440px',
        "476": '476px',
        "100": '100vh',
        "193": '193px',
      }, 
      height: {
        "99": '694px',
        "48": '48px',
        "106": '106px',
        "460": '460px',
        "115":'115px'
      }, 
      variants: {
        backgroundColor:['responsive', 'hover', 'focus', 'active', ]
      },
      leading: {
      'base': '79px',
      'norm': '6px'
      }
     
         },
  },
 
  plugins: [
    require('tw-elements/dist/plugin')
  ],
}