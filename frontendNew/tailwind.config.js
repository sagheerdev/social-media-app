/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{vue,js}",
    "./components/**/*.{vue,js}",
    "./node_modules/flowbite/**/*.js",
  ],
  plugins: [require("flowbite/plugin")],
  theme: {
    colors: {
      "custom-blue": "#1fb6ff",
      blue: "#1fb6ff",
      purple: "#7e5bef",
      pink: "#ff49db",
      orange: "#ff7849",
      green: "#13ce66",
      yellow: "#ffc82c",
      "gray-dark": "#273444",
      gray: "#8492a6",
      "gray-light": "#d3dce6",
      "blue-def": "#083D77B2",
      "gray-section": "#E9F1FA",
      "nDark-blue": "#083D77",
      "nNav-shadow": "#CAD1C94D",
      "nBox-shadow": "#00ABE440",
    },

    fontFamily: {
      sans: ["Graphik", "sans-serif"],
      serif: ["Merriweather", "serif"],
      Poppins: ["Poppins", "sans-serif"],
      Poetsen: ["PoetsenOne", "sans-serif"],
      Roboto: ["Roboto", "sans-serif"],
    },

    extend: {
      spacing: {
        "8xl": "96rem",
        "9xl": "128rem",
      },
      borderRadius: {
        "4xl": "2rem",
      },
      boxShadow: {
        custom: "0px 4px 10px rgba(0, 171, 228, 0.25)",
        "shadow-form": "0px 4px 10px rgba(24, 76, 52, 0.25)",
        "order-border": " rgba(0, 171, 228, 0.5);",
      },
    },
  },
  plugins: [],
};
