// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr:true,
  
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/google-fonts",
    "@pinia/nuxt",
    "nuxt-icon",
    "@pinia-plugin-persistedstate/nuxt",
  ],

  runtimeConfig: {
    API_BASE: process.env.API_BASE,
    public: {
      API_BASE: process.env.API_BASE,
    },
  },

  app: {
    pageTransition: { name: "page", mode: "out-in" },
  },
  googleFonts: {
    // https://google-fonts.nuxtjs.org/downloading
    display: "swap",
    download: false,
    inject: true,
    overwriting: false,
    subsets: [],
    prefetch: true,
    preconnect: true,
    stylePath: "css/tailwind.css",
    families: {
      Inter: {
        wght: [100, 200, 300, 400, 500, 600, 700, 800, 900],
      },
      Poppins: {
        wght: [100, 200, 300, 400, 500, 600, 700, 800, 900],
      },
    },
  },
});
