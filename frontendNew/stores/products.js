import { defineStore } from "pinia";

export const useProductStore = defineStore("product-store", () => {
  const productsList = ref([]);

  const selectedProduct = ref({
    name: null,
    description: null,
    features: [null],
    image: null,
    prices: [
      {
        name: null,
        amount: null,
        product_id: null,
        api_service_id: null,
        api_quantity: null,
        strip_price_id: null,
      },
    ],
    image_data: null,
  });

 const AddNewProduct = () => {
   selectedProduct.value = {
     name: "",
     description: "",
     features: [""],
     image: "",
     prices: [
       {
         name: "",
         amount: "",
         product_id: "",
         api_service_id: "",
         api_quantity: "",
         strip_price_id: "",
       },
     ],
     image_data: "",
   };
 };

  const getProductsList = async () => {
    const res = await post("/getProducts");
    console.log("response of prodcut", res);
    productsList.value = res.data.products; //productsList.value = res?.data?.products || [];
  };

  const setSelectedProduct = async (product) => {
    selectedProduct.value = product;
  };

  const AddProduct = async () => {
    const res = await post("/addProduct", { ...selectedProduct.value });
    console.log("response of Add product", res);
    return res;
  };

  const UpdateProduct = async () => {
    const res = await post(`/updateProduct/${selectedProduct.value.id}`, {
      ...selectedProduct.value,
    });
    console.table("updating product", res);
    return res;
  };

  return {
    productsList,
    selectedProduct,
    AddProduct,
    getProductsList,
    UpdateProduct,
    setSelectedProduct,
    AddNewProduct 
    
  }
});



