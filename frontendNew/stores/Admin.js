import { defineStore } from "pinia";

export const useAdminStore = defineStore("admin", {
  state() {
    return {
      currentUser: null,
      userData: {
        email: " ",
        password: " ",
      },
    };
  },

  actions: {
    async Login() {
      const response = await post("/login", this.userData);
      this.currentUser = response.data;
      return response;
    },
    async LogOut() {
      const response = await post("/logout",this.userData);
      this.currentUser.key = null;
      console.log(response, "logout response");
      return response;
    },
  },
  persist: true,
});
