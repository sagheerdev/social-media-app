import { defineStore } from 'pinia'
export const useOrderStore = defineStore('orderStore',{
  state: () => {
    return {
      selectedPrice: null,
      selectedProduct: null,
    }
  },
  getters: {
    getSelectedProduct(state) {
      return state.selectedProduct;
    },
    getSelectedPrice(state) {
      return state.selectedPrice;
    }
  },
  actions: {
    setSelectedProduct(product) {
      this.selectedProduct = product
      this.selectedPrice = product?.prices[0]
    },
    setSelectedPrice(price) {
      this.selectedPrice = price

    }
  },
});





