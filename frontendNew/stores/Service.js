import { defineStore } from "pinia";
export const useServiceStore = defineStore("servicestore", () => {
  const selectedPrice = ref(null);
  const selectedProduct = ref(null);

  const PriceGetter = computed(
    () => selectedPrice.value,
    selectedProduct.value
  );

  //Getters
  function setSelectedProduct(product) {
    selectedProduct.value = product;
    selectedPrice.value = product?.prices[0];
  }

  function setSelectedPrice(price) {
    console.log(price);
    selectedPrice.value = price;
  }

  return {
    selectedPrice,
    selectedProduct,
    PriceGetter,
    setSelectedPrice,
    setSelectedProduct,
  };
});
