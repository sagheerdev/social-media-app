
import { useAdminStore } from "~~/stores/Admin.js";
import { storeToRefs } from "pinia";
/**Login Api */

const post = async (url, data, headers) => {
  const store = useAdminStore();
  const { currentUser } = storeToRefs(store);
  const baseUrl = getApiPath("/api");
  const fullUrl = baseUrl + url;
  const reqsdata = {
    token: currentUser.value?.token,
    ...data,
  };
  console.log(currentUser.value,fullUrl,reqsdata ,"api response")
  const Response = await fetch(fullUrl, {
    method: "POST",
    body: JSON.stringify({ ...reqsdata }),
    headers: {
      "content-type": "application/json",
      ...headers,
    },
  });
  const JsonResponse = await Response.json();
  return { data: JsonResponse, code: Response.status };
};

const getApiPath = (url = "") => {
  const config = useRuntimeConfig().public;
  const baseUrl = config.API_BASE;
  return `${baseUrl}${url}`;
};

const getImageUrl = (imgPath = "") => {
  const config = useRuntimeConfig().public;
  const baseUrl = config.API_BASE;
  return `${baseUrl}${imgPath}`;
};

const del = async (url, data, headers) => {
  const config = useRuntimeConfig().public;
  
  const baseUrl = config.API_BASE;
  const fullUrl = `${baseUrl}/api${url}`;
  const reqsdata = {
    ...data,
    token: JSON.parse(localStorage.getItem("user-info"))?.token,
  };
  const Response = await fetch(fullUrl, {
    method: "DELETE",

    headers: {
      "content-type": "application/json",
      ...headers,
    },
  });
  const JsonResponse = await Response.json();

  return { data: JsonResponse, code: Response.status };
};
export { post, del, getApiPath, getImageUrl };
