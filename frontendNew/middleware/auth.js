import { useAdminStore } from "../stores/Admin.js";
import { storeToRefs } from "pinia";

export default defineNuxtRouteMiddleware((to, from) => {
  const store = useAdminStore();
  const { currentUser } = storeToRefs(store);

  const isAuthenticated = () => {
    const userInfo = currentUser.value;
    return userInfo && userInfo.key;
  };

  if (isAuthenticated() && to.path === "/login") {
    return navigateTo("/admin");
  }

  if (!isAuthenticated() && to.path.startsWith("/admin")) {
    return navigateTo("/login");
  }
});
