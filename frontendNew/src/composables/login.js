import { getApiPath } from "./apiroute";

const post = async (url, data, headers) => {
  const baseUrl = getApiPath("/api");
  const fullUrl = baseUrl + url;
  const requestData = {
    ...data,
    token: localStorage.getItem("admin-info")
      ? JSON.parse(localStorage.getItem("admin-info"))
      : null,
  };
  const response = await fetch(fullUrl, {
    method: "POST",
    body: JSON.stringify(requestData),
    headers: {
      ...headers,
      "content-type": "application/json",
    },
  });

  const jsonResponse = await response.json();

  return { data: jsonResponse, code: response.status };
};
export { post };
