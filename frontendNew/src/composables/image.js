const getImageUrl = (imgPath = "") => {
  if (window.location.hostname.includes("localhost")) {
    return `http://127.0.0.1:8000${imgPath}`;
  }
  return `https://likes4u.co/backend${imgPath}`;
};
export { getImageUrl };
