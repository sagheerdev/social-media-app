import { defineStore } from 'pinia'
export const useOrderStore = defineStore('orderStore', {
  state: () => {
    return {
      selectedPrice: null,
      selectedProduct: null,
      orderDialog: false
    }
  },
  getters: {
    getOrderDialog(state) {
      return state.orderDialog;
    },
    getSelectedProduct(state) {
      return state.selectedProduct;
    },
    getSelectedPrice(state) {
      return state.selectedPrice;
    }
  },
  actions: {
    toggleOrderDialog() {
      this.orderDialog = !!!this.orderDialog
    },
    setSelectedProduct(product) {
      this.selectedProduct = product
      this.selectedPrice = product?.prices[0]
    },
    setSelectedPrice(price) {
      this.selectedPrice = price

    }
  },
})
export default useOrderStore