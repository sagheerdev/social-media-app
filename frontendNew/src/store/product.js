import { defineStore } from 'pinia'
import { post } from '../composables/upload'
export const useProductStore = defineStore('productStore', {
  state: () => {
    return {
      productDialog: false,
      selectedProduct: {},
    }
  },
  getters: {
    getproductDialog(state) {
      return state.productDialog;
    }
  },
  actions: {
    toggleProductDialog() {
      this.productDialog = !!!this.productDialog
    },
    async AddProduct() {
      return await post('/addProduct', this.selectedProduct)
    },
    // Add Feature
    addFeature() {
      this.selectedProduct.features.push("");
    },
    removeFeature(index) {
      this.selectedProduct.features.splice(index, 1);
      console.log(index)
    },
    // Add price name
    addPrice() { this.selectedProduct.prices.push({ name: null, amount: null }) },
    removePrice(index) {
      this.selectedProduct.prices.splice(index, 1)
    },
    setimage(image) {
      this.image = image;
    },
    setname(name) {
      this.name = name;
    },
    setdescription(description) {
      this.description = description;
    },
    setfeatures(features) {
      this.features = features;
    },
    setprices(prices) {
      this.prices = prices;
    },
    setSelectedProduct(product) {
      this.selectedProduct = product
    },
  },

})
export default useProductStore